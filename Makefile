CXX = g++
INCLUDE = -I./include
CXXFLAGS = $(INCLUDE) -O0 -ggdb -pipe -std=c++98
OBJDIR = ./src
LIBS = -lboost_thread -lboost_system


OBJS = Indexer.o main.o WordIndex.o
OBJS_OUT = $(addprefix $(OBJDIR)/,$(OBJS))
EXEC = TextfileIndexer

all: $(EXEC)

$(EXEC): $(OBJS_OUT)
	$(CXX) $(CXXFLAGS) -o $(EXEC) $(OBJS_OUT) $(LIBS)

$(OBJDIR)/%.o: %.cpp
	$(CXX) $(CXXFLAGS) -c $< -o $@

clean:
	rm $(EXEC) $(OBJS_OUT)
