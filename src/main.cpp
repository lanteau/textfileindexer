#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include "Indexer.h"

int main(int argc, char *argv[])
{
	if(argc < 3)
	{
		std::cout << "Usage: " << argv[0] << " <number of threads> <text file>"
			<< std::endl;
		return 1;
	}

	std::string filename = argv[2];

	std::istringstream ss(argv[1]);
	int numThreads;
	if(!(ss >> numThreads))
	{
		std::cerr << "Invalid number: " << argv[1] << std::endl;
		return 1;
	}

	Indexer ind(numThreads, filename);

	ind.printIndex();

	return 0;
}