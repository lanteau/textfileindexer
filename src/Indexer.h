/* Indexer - Logan Anteau
*
* This class will actually perform all the indexing of the text file
*
*/
#ifndef _INDEXER_H
#define _INDEXER_H

#include <string>
#include <vector>
#include <utility>
#include "WordIndex.h"

class Indexer;

struct WorkPayLoad {
	WordIndex *wordIndex;
	std::string data;
	int offset;
};

class Indexer
{
public:
	typedef std::vector<int> IntVec;

	Indexer(unsigned int numThreads = 1, std::string file = "");
	WordIndex* getMap();
	std::vector<int> offsetsOf(const std::string word);
	void printIndex();

private:
	WordIndex m_words;
	std::string m_fileData;
	std::vector<std::pair<int,int> > m_assignments;

	void assignWork(unsigned int numThreads);

	void startWork();
	//void buildIndex();

// protected:
// 	friend void* do_action(void* arg)
// 	{
// 		WorkPayLoad *work = static_cast<WorkPayLoad*>(arg);
// 		work->this->buildIndex(work->workPair);
// 	}

};

#endif /* _INDEXER_H */