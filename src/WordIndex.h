/* WordIndex - Logan Anteau
*This is a thread-safe data structure that will store the words and
* their offsets
*/
#ifndef _WORDINDEX_H
#define _WORDINDEX_H
#include <map>
#include <string>
#include <boost/thread/shared_mutex.hpp>
#include <boost/noncopyable.hpp>
#include <boost/shared_ptr.hpp>

class WordIndex : boost::noncopyable
{
	typedef boost::shared_ptr<boost::shared_mutex> mutexPtr;

	std::map<std::string, std::vector<int> > m_map;
	std::map<std::string, mutexPtr> m_valueAccess;
	boost::shared_mutex m_mutex;

public:
	bool find(std::string str);
	bool add(std::string str, int pos);
	std::vector<int> get(std::string str);

	std::map<std::string, std::vector<int> >::const_iterator begin();
	std::map<std::string, std::vector<int> >::const_iterator end();

};

#endif /* _WORDINDEX_H */