#include "WordIndex.h"

bool WordIndex::find(std::string str)
{
	boost::shared_lock< boost::shared_mutex > lock(m_mutex);
	if(m_map.find(str) != m_map.end())
		return true;
	return false;
}

bool WordIndex::add(std::string str, int pos)
{
	boost::upgrade_lock< boost::shared_mutex > lock(m_mutex);

	//setting
	if(m_map.find(str) != m_map.end())
	{
		boost::upgrade_lock<boost::shared_mutex> valueLock(*m_valueAccess[str]);
		boost::upgrade_to_unique_lock< boost::shared_mutex >
			valueUniqueLock(valueLock);
		m_map.at(str).push_back(pos);

		// When adding to vector that already exists, sort it
		sort( m_map.at(str).begin(), m_map.at(str).end() );
	}
	//adding
	else
	{
		boost::upgrade_to_unique_lock< boost::shared_mutex > uniqueLock(lock);
		m_valueAccess.insert(std::pair < std::string, 
			boost::shared_ptr<boost::shared_mutex> > 
			(str, boost::shared_ptr<boost::shared_mutex>
				(new boost::shared_mutex())));
		m_map[str].push_back(pos);

	}
}

std::vector<int> WordIndex::get(std::string str)
{
	boost::shared_lock< boost::shared_mutex > lock(m_mutex);
	std::vector<int> result;

	if(find(str))
	{
		result = m_map[str];
	}

	return result;
}

std::map<std::string, std::vector<int> >::const_iterator WordIndex::begin()
{
	return m_map.begin();
}

std::map<std::string, std::vector<int> >::const_iterator WordIndex::end()
{
	return m_map.end();
}
