#include "Indexer.h"
#include <fstream>
#include <sstream>
#include <boost/algorithm/string.hpp>
#include <boost/thread.hpp>
#include <boost/bind.hpp>

Indexer::Indexer(unsigned int numThreads, std::string file)
{
	std::ifstream input(file.c_str(), std::ifstream::in);
	std::stringstream buffer;
	buffer << input.rdbuf();

	m_fileData = buffer.str();

	assignWork(numThreads);
	startWork();

	input.close();

}

WordIndex* Indexer::getMap()
{
	return &m_words;
}

std::vector<int> Indexer::offsetsOf(std::string word)
{
	boost::algorithm::to_lower(word);

	if(m_words.find(word))
		return m_words.get(word);

	std::vector<int> emptyVector;
	return emptyVector;

}

void Indexer::assignWork(unsigned int numThreads)
{
	int byteCount = m_fileData.length();

	// Initial rough outline for work divisions
	int bytesPerThr = byteCount / numThreads; 

	int pos = bytesPerThr - 1;
	int start = 0;
	for(size_t i = 0; i < numThreads; i++)
	{
		while(pos != m_fileData.length() - 1 && m_fileData.at(pos) != ' ')
			pos++;
		std::pair<int,int> *assignment = new std::pair<int, int>(start, pos);
		m_assignments.push_back(*assignment);
		start = pos + 1;
		int remainingBytes = byteCount - (pos + 1);

		if((numThreads - (i+1) > 0))
			bytesPerThr = remainingBytes / (numThreads - (i+1));
		else
			break;

		pos += bytesPerThr;
		if(pos > m_fileData.length() - 1)
			pos = m_fileData.length() - 1;

	}
}

void buildIndex(int offset, std::string work, WordIndex *wordIndex)
{	
	std::string line;
	int lineOffset = offset;
	std::stringstream ss(work);

	while (ss.good())
	{
		getline(ss, line);

		int lineSize = line.length() + 1; // Add 1 for \n character

		int pos = 0;
		std::string word;

		int absLinePos = 0;
		while(pos != std::string::npos)
		{
			pos = line.find(" ");
			word = line.substr(0, pos);

			//Remove leading punctation
			while(!isalpha(word[0]) && word.length() > 1)
			{
				word = word.substr(1, std::string::npos);
			}


			while(!isalpha(word[word.length() - 1]) && word.length() > 0)
			{
				word = word.substr(0, word.length() - 1);
			}


			boost::algorithm::to_lower(word);

			//Trim word off front
			line = line.substr(pos + 1, std::string::npos);

			// Line offset is the position in file of first character in this line
			// absLinePos is absolute position in this line of this word
			wordIndex->add(word, lineOffset + absLinePos);

			absLinePos += pos + 1; // +1 to account for the spaces we drop

		}
		lineOffset += lineSize;
	}
	return;
}

void Indexer::startWork()
{
	boost::thread_group tgroup;

	for(size_t i = 0; i < m_assignments.size(); i++)
	{
		std::string work = m_fileData.substr(m_assignments.at(i).first, 
			m_assignments.at(i).second - m_assignments.at(i).first);

		tgroup.create_thread( boost::bind(&buildIndex, 
			m_assignments.at(i).first, work, &m_words));
	}

	tgroup.join_all();

}

void Indexer::printIndex()
{
	for(std::map<std::string, std::vector<int> >::const_iterator it = m_words.begin();
		it != m_words.end(); it++)
	{
		std::cout << "Key: " << it->first << std::endl << "Offsets: ";
		for(size_t i = 0; i < it->second.size(); i++)
		{
			std::cout << it->second.at(i);
			if(i != (it->second.size() - 1))
				std::cout << ", ";
		}
		std::cout << std::endl << std::endl;
	}
}
